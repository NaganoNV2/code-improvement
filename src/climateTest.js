/* eslint-disable no-unused-vars */
function correctFunction(arg1, arg2, arg3, arg4) {
    return arg1 + arg2 + arg3 + arg4;
}

// Uncomment to test incorrect code
/*
function incorrectFunction(arg1, arg2, arg3, arg4, arg5) {
    return arg1 + arg2 + arg3 + arg4 + arg5;
}
*/

// Code Climate configuration: argument-count threshold is 4.
