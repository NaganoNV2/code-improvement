// Correct Code
const user = 'John Doe'; // 'let' is replaced with 'const' as 'user' is never reassigned
if (user) {
    // Replace 'console.log' with a different logging mechanism or function call if necessary
    console.log(user);
}
