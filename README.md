# プロジェクトツール導入ガイド

##　新HAPでのコード品質の役割理解
[新HAPとコード品質の概要](https://gitlab.com/dev-krc/training/materials/uploads/c4e445b194ccce646feca4c66e4e058d/%E6%96%B0HAP%E3%81%A8%E3%82%B3%E3%83%BC%E3%83%89%E5%93%81%E8%B3%AA.pdf)

## Prettierの導入方法

1. パッケージのインストール
   まず、Prettierをプロジェクトにインストールします。Node.jsが必要なので、まだインストールされていない場合は、それを先に行ってください。

```
npm install --save-dev prettier
```

または、yarnを使用している場合:

```
yarn add --dev prettier
```

2. 設定ファイルの作成
   プロジェクトのルートに.prettierrcファイルを作成し、必要な設定を記述します。例えば:

```
{
    "semi": true,
    "singleQuote": true
}

```

3. package.jsonにスクリプトを追加（オプショナル）
   より簡単にコードをフォーマットできるように、package.jsonにスクリプトを追加することもできます。

```
"scripts": {
"format": "prettier --write \"src/**/*.{js,jsx,ts,tsx,json,css,scss,md}\""
}
```

これにより、npm run formatやyarn formatでプロジェクト全体のコードを簡単にフォーマットできるようになります。

ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

## EditorConfigの導入方法

1. EditorConfigファイルの作成
   プロジェクトのルートディレクトリに.editorconfigという名前のファイルを作成します。これが、EditorConfigの設定を含むファイルになります。

2. 設定の記述
   .editorconfigファイル内に、プロジェクトで使用するコーディングスタイルに関する設定を記述します。例えば以下のような内容を含めることができます：

```
# editorconfig.org
root = true

[*]
charset = utf-8
indent_style = space
indent_size = 4
end_of_line = crlf
insert_final_newline = true
trim_trailing_whitespace = true

[*.md]
insert_final_newline = false
trim_trailing_whitespace = false

```

3. エディタやIDEの設定
   使用しているエディタやIDEがEditorConfigをネイティブでサポートしている場合は、特別な設定は必要ありません。サポートしていない場合は、対応するプラグインをインストールする必要があります。

多くのエディタやIDEはEditorConfigをサポートしているため、公式サイトのEditorConfigのダウンロードページ
で詳細を確認し、必要に応じてプラグインをインストールしてください。

4. チームメンバーへの共有
   .editorconfigファイルはプロジェクトの一部としてリポジトリにコミットすることが望ましいです。これにより、プロジェクトに参加する全ての開発者が同じコーディングスタイルを使用することが保証されます。

ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

## ESLintの導入方法

# JavaScript用のESLintインストール

1. ESLintのインストール
   まずは、プロジェクトにESLintをインストールする必要があります。Node.jsが必要なので、未インストールの場合は先に行ってください。

```
npm install eslint --save-dev
```

または、yarnを使用している場合は以下のようにします。

```
yarn add eslint --dev
```

# TypeScript用のESLintインストール

```
npm install eslint @typescript-eslint/parser @typescript-eslint/eslint-plugin --save-dev
```

または、yarnを使用する場合：

```
yarn add eslint @typescript-eslint/parser @typescript-eslint/eslint-plugin --dev
```

2. ESLintの設定
   ESLintを初めてプロジェクトに導入する場合、設定ファイルを作成することが推奨されます。以下のコマンドを実行して設定プロセスを開始します。

```
npx eslint --init
```

このコマンドはいくつかの質問を行い、あなたのプロジェクトに合ったESLintの設定を生成します。生成される設定ファイルは通常、.eslintrc .eslintrc.json、または.eslintrc.ymlという名前です。

3. ESLintルールの設定
   ESLintの設定ファイル内で、特定のルールを有効化、無効化、またはカスタマイズすることができます。例えば：

```
{
    "parserOptions": {
        "ecmaVersion": 2023,
        "sourceType": "module",
        "ecmaFeatures": {
            "jsx": true
        }
    },
    "extends": ["airbnb-base"],
    "rules": {
        "eqeqeq": "warn",
        "curly": "error",
        "quotes": ["error", "single"],
        "no-extra-semi": "error",
        "linebreak-style": ["error", "windows"], // 追加されたルール: Windowsの改行コード(CRLF)を許可する
        "indent": ["error", 4] // 追加されたルール: 4スペースのインデントを許可する
    }
}
```

# TypeScript用

```
{
    "parser": "@typescript-eslint/parser",
    "extends": [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended"
    ],
    "rules": {
        // ルールの設定例
    }
}
```

4. package.jsonにスクリプトを追加
   頻繁にESLintを実行するために、package.jsonにスクリプトを追加することが便利です。

```
"scripts": {
　"lint": "eslint \"src/**/*.{js,jsx,ts,tsx}\" --quiet --fix"
}
```

これにより、npm run lintやyarn lint
でプロジェクト全体を簡単にlintできるようになります。

5. IDEとの統合（オプショナル）
   多くのIDEやエディタにはESLintのプラグインがあり、コードを書いている間にリアルタイムでlintエラーや警告を表示できます。使用しているエディタに合ったプラグインをインストールし、適切に設定することで、開発プロセスが大幅にスムーズになります。

ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

## StyleLint の導入方法

StyleLintの導入方法

1. StyleLintのインストール
   プロジェクトにStyleLintをインストールします。Node.jsが必要なので、未インストールの場合は先に行ってください。

```
npm install stylelint --save-dev
```

または、yarnを使用している場合は以下のようにします。

```
yarn add stylelint --dev
```

2. StyleLintの設定ファイルの作成
   プロジェクトのルートディレクトリに.stylelintrc、.stylelintrc.json、.stylelintrc.yaml、または.stylelintrc.jsという名前の設定ファイルを作成します。

3. スタイルルールの設定
   StyleLintの設定ファイルに、適用したいスタイルルールを記述します。例えば、以下のような設定が可能です：

```
{
    "rules": {
        "block-no-empty": true,
        "color-no-invalid-hex": true,
        "declaration-block-no-duplicate-properties": true,
        "declaration-block-no-shorthand-property-overrides": true,
        "font-family-no-duplicate-names": true,
        "font-family-no-missing-generic-family-keyword": true,
        "function-linear-gradient-no-nonstandard-direction": true,
        "string-no-newline": true,
        "unit-no-unknown": true
    }
}
```

4. package.jsonにスクリプトを追加
   頻繁にStyleLintを実行するために、package.jsonにスクリプトを追加することが便利です。

```
"scripts": {
 "lint-css": "stylelint \"src/**/*.{css,scss}\" --fix"
}
```

これにより、npm run lint:cssやyarn lint:css
でプロジェクト全体のCSSを簡単にlintできるようになります。

5. IDEとの統合（オプショナル）
   多くのIDEやエディタにはStyleLintのプラグインがあり、コーディング中にリアルタイムでlintエラーや警告を表示できます。使用しているエディタに合ったプラグインをインストールし、適切に設定することで、開発プロセスが大幅にスムーズになります。

ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

## CodeClimate の導入方法

CodeClimateの導入方法()

1. CodeClimateアカウントの作成
   まず、CodeClimateにアクセスしてアカウントを作成します。GitHub、GitLab、Bitbucketなどのリポジトリを連携できます。

2. リポジトリの追加
   CodeClimateダッシュボードで、分析したいリポジトリを選択して追加します。これにより、選択したリポジトリのコード品質の分析が開始されます。

3. 設定ファイルの追加（オプショナル）
   プロジェクトのルートに.codeclimate.ymlファイルを作成して、CodeClimateの挙動をカスタマイズすることができます。このファイルでは、チェックするファイルの種類、除外するファイル、使用するプラグインなどを指定できます。

例えば、以下のように設定することができます：

```
version: '2'
checks:
    argument-count:
        enabled: true
        config:
            threshold: 4
```

ローカル環境でのCLI導入

Dockerのインストール:
Code Climate CLIはDockerコンテナ内で動作するため、ローカルマシンにDockerをインストールしてください。

```
curl -L https://github.com/codeclimate/codeclimate/archive/master.tar.gz | tar xvz
cd codeclimate-* && sudo make install
```

Dockerイメージのプル

```
docker pull codeclimate/codeclimate
```

プロジェクトのセットアップ:
プロジェクトのルートディレクトリに移動し、以下のコマンドを実行して、Code Climateエンジンをインストールします:

```
codeclimate engines:install
```

4. package.jsonにスクリプトを追加（オプショナル）

```
"climate": "codeclimate analyze"
```

ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

## git commitでformatとlintが実行され、lintでエラーが出た場合はコミットできないようにする

ESLintのインストール
ESLintの設定ファイル.eslintrc
の作成:
AirbnbのESLintルールセットのインストール:

```
yarn add eslint-config-airbnb-base --dev
```

.eslintignoreの作成:

```
node_modules dist .github
```

Prettierのインストール:

Prettierの設定ファイル.prettierrc
の作成:

.prettierignoreの作成:

```
dist .github
```

lint-stagedのインストール:

```
yarn add lint-staged --dev
```

lint-stagedの設定ファイル.lintstagedrcの作成:

```
{
  "*.{js,ts}": [
    "npx eslint --fix",
    "npx prettier --write",
    "npx jest"
  ]
}
```

huskyのインストール:

```
yarn add husky --dev
```

huskyの設定ファイル.huskyrcの作成 (または.husky
フォルダ内の設定ファイルを編集):

```
{ "hooks": { "pre-commit": "lint-staged", "commit-msg": "commitlint -E HUSKY_GIT_PARAMS" }}
```

huskyの初期化:

```
npx husky-init && yarn
```

.husky/pre-commitの編集:

```
#!/usr/bin/env sh
. "$(dirname -- "$0")/_/husky.sh"

yarn lint-staged
```

package.jsonにlint-stagedコマンドを追加:

```
{ "scripts": { ... "lint-staged": "lint-staged" }}
```

参照:https://zenn.dev/ianchen0419/articles/a70d181de74df0

